from django.shortcuts import render

# Create your views here.
def homePageView(request):
    return render(request, "base_home.html")

def profileView(request):
    return render(request, "base_profile.html")

def interestView(request):
    return render(request, "base_interest.html")

def waifuView(request):
    return render(request, "base_waifu.html")